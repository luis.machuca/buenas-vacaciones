# Proyecto Buenas Vacaciones

El siguiente proyecto se encuentra alojado en GitLab, para comenzar a trabajar con repositorios GIT y  dominar sus principios principios básicos.

# Como descargar este proyecto

Existen dos formas de poder comenzar a trabajar con este proyecto, se recomienda que abra una cuenta en GitLab, dado que los accesos al repositorio son privados:

A-. Clonar (Recomendado):

1.- Hacer clic en el botón clonar, ubicado en el lado superior derecha, botón azul.

2.- Puede copiar la dirección web y pegarla en el terminal (cmd) ya sea de windows o linux, en la carpeta de su preferencia; o bien, abrirla directamente con Visual Studio Code (altamente recomendado), también se le pedirá carpeta de destino.

B.- Descargar (Poco recomendado):

1.- Hacer clic en el boton que simboliza la descarga de archivos, la lado del botón "Clonar".

2.- Puede escoger entre distintos formatos de compresión (zip, tar, etc)


# Como subir los cambios al repositorio

De momento, estos son los comandos más importantes al momento de hacer un  push al repositorio (en otras palabras, subir cambios al repositorio), todos realizados en el cmd o terminal en la raiz del proyecto:

- git status: muestra los archivos sin commit
- git add (.): permite especificar los archivos que se mandarán o modificarán el en repositorio, el punto simboliza que se subirán todos los archivos con cambios realizados.
- git commit -m "mensaje": permite especificar un mensaje. Se recomienda que este mensaje sea relacionado con los cambios realizados al sitio.
- git push -u origin main: sube los archivos agregados al commit, es importante especificar la rama a la que se sube los cambios.

Mas información de GIT en la siguiente dirección web: 

https://rogerdudler.github.io/git-guide/index.es.html